<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', 'CrudController@index');
// Route::get('/create', 'CrudController@create');
// Route::post('/create/store', 'CrudController@store');
// Route::get('/edit/{id}', 'CrudController@edit');
// Route::post('/edit/update/{id}', 'CrudController@update');
// Route::delete('/delete/{id}', 'CrudController@destroy');

Route::get('/', 'CrudController@index');
Route::get('/create', 'CrudController@create');
Route::post('/create/store', 'CrudController@store');
Route::get('/edit/{id}', 'CrudController@edit');
Route::put('/update-pegawai', 'CrudController@update');
Route::delete('/delete/{id}', 'CrudController@destroy');

Route::get('/export', 'CrudController@export');