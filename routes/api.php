<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/api/user', function (Request $request) {
    return $request->user();
});

// Route::get('/pegawai', 'ApiController@index')->name('');
// Route::post('/pegawai', 'ApiController@create')->name('');
// Route::put('/pegawai/{id}', 'ApiController@edit')->name('');
// Route::put('/pegawai/{id}', 'ApiController@update')->name('');
// Route::delete('/pegawai/{id}', 'ApiController@destroy')->name('');

Route::get('/pegawai', [ApiController::class, 'index'])->name('home');
Route::post('/tambah-pegawai', [ApiController::class, 'create'])->name('tambah_data');
Route::delete('/hapus-pegawai/{id}', [ApiController::class, 'destroy'])->name('hapus-pegawai');
Route::post('/update-pegawai/{id}', [ApiController::class, 'update'])->name('edit_data');