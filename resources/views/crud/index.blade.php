@extends('layouts.dashboard')

@section('header')
<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboards</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pegawai</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="container-fluid mt--6">
    <!-- Table -->
    <div class="row">
        <div class="col">
            <div class="text-right">
                <form action="{{url('/export')}}" enctype="multipart/form-data" class="d-inline">
                    <button class="btn btn-sm btn-secondary  mb-3" type="submit">Export</button>
                </form>
                {{-- <a href="{{ url('create')') }}" class="btn btn-sm btn-success mb-3">Tambah Data</a> --}}
                <form action="{{ url('/create') }}" class="d-inline">
                    <button type="submit" class="btn btn-sm btn-success mb-3">Tambah Data</button>
                </form>
            </div>
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif
                    <h3 class="mb-0">Tugas CRUD 1</h3>
                    <br>
                    <table class="table" id="datatable-basic">
                        <tr>
                            <td class="text-center" width="4%">No</td>
                            <td class="text-center" width="10%">Nama</td>
                            <td class="text-center" width="10%">Alamat</td>
                            <td class="text-center" width="10%">Jenis Kelamin</td>
                            <td class="text-center" width="10%">Posisi</td>
                            <td class="text-center" width="10%">Aksi</td>
                        </tr>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($pegawai as $key=>$row)
                        <tr>
                            <td class="text-center">{{ $no++ }}</td>
                            <td class="text-center">{{ $row->nama }}</td>
                            <td class="text-center">{{ $row->alamat }}</td>
                            <td class="text-center">{{ $row->jenis_kelamin }}</td>
                            <td class="text-center">{{ $row->posisi }}</td>
                            <td class="text-center">
                                <a href="{{ url('/edit', $row->id) }}" class="btn btn-sm btn-info">Edit</a>
                                <form action="{{ route('hapus-pegawai', $row->id) }}" method="post" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" onclick="return confirm('Anda yakin menghapus?');"  class="btn btn-sm btn-danger">Hapus</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection