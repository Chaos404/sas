@extends('layouts.dashboard')

@section('header')
<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboards</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pegawai</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="container-fluid mt--6">
    <!-- Table -->
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif
                    <h3 class="mb-0">Tugas CRUD 1</h3>
                    <br>
                    <form action="{{ route('tambah_data') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nama</label>
                            <div class="input group"></div>
                            <input type="text" class="form-control" name="nama" id="nama">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <div class="input group"></div>
                            <input type="text" class="form-control" name="alamat" id="alamat">
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <div class="input group"></div>
                            {{-- <input type="text" class="form-control" name="jenis_kelamin" id=""> --}}
                            <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                <option value="">-- Pilih Kelamin --</option>
                                <option value="Pria">Pria</option>
                                <option value="Wanita">Wanita</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Posisi</label>
                            <div class="input group"></div>
                            <input type="text" class="form-control" name="posisi" id="posisi">
                        </div>
                        <button type="submit" class="btn btn-info" style="float: right">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection