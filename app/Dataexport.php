<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pegawai;
use Maatwebsite\Excel\Concerns\FromCollection;

class Dataexport implements FromCollection
{
    public function collection(){
        return Pegawai::all();
    }
}
