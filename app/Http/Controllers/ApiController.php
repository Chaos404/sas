<?php

namespace App\Http\Controllers;

use App\Pegawai;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pegawai = DB::select('SELECT public."show"()');
        $fixed = array();
        foreach ($pegawai as $row){
            $isi = str_replace(['(', ')', '"'], '', $row->show);
            $array = explode(',', $isi);
            $data = array(
                    "id" => $array[0],
                    "nama" => $array[1],
                    "alamat" => $array[2],
                    "jenis_kelamin" => $array[3],
                    "posisi" => $array[4]
                );
            $fixed[] = $data;
        }
        return json_encode($fixed);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'jenis_kelamin' => ['required'],
            'posisi'=> ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $pegawai = DB::statement('SELECT public."insert"(:nama, :alamat, :jenis_kelamin, :posisi)', [
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'jenis_kelamin' => $request->jenis_kelamin,
                'posisi' => $request->posisi,
            ]);

            if ($pegawai) {
                return redirect('/')->with('success', 'Data Berhasil Ditambahkan');
            } else {
                return redirect('/')->with('errors', 'Data Gagal Ditambahkan');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required', 'numeric', 'exists:pegawai,id'],
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'jenis_kelamin' => ['required'],
            'posisi'=> ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $pegawai =  DB::statement('SELECT public."update"(:id, :nama, :alamat, :jenis_kelamin, :posisi)', [
                'id' => $request->id,
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'jenis_kelamin' => $request->jenis_kelamin,
                'posisi' => $request->posisi,
            ]);

            if ($pegawai) {
                return redirect('/')->with('success', 'Data Berhasil Ditambahkan');
            } else {
                return redirect('/')->with('errors', 'Data Gagal Ditambahkan');
            }
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = DB::delete('SELECT public."delete"(:id)', [
            'id' => $id
        ]);
        
        if ($pegawai) {
            return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        } else {
            return redirect()->back()->with('errors', 'Data Gagal Dihapus');
        }
    }
}
