<?php

namespace App\Http\Controllers;

use App\Dataexport;
use App\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Excel;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('crud.index', [
        //     'pegawai' => Pegawai::all()
        // ]);

        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://127.0.0.1:8001/api/pegawai');
        $pegawai = json_decode($request->getBody()->getContents());

        return view('crud.index', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'jenis_kelamin' => ['required'],
            'posisi'=> ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $client = new Client();
            $response = $client->post('http://127.0.0.1:8001/api/tambah-pegawai', ['form_params' =>
                [
                    'nama' => $request->nama,
                    'alamat' => $request->alamat,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'posisi' => $request->posisi
                ]
            ] );

            if ($response) {
                return redirect('/')->with('success', 'Data Berhasil Ditambahkan');
            } else {
                return redirect('/')->with('errors', 'Data Gagal Ditambahkan');
            }
            // return "GAS";
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pegawai = Pegawai::where('id',$id)->firstOrFail();
        return view('crud.edit', compact('pegawai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required', 'numeric', 'exists:pegawai,id'],
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'jenis_kelamin' => ['required'],
            'posisi'=> ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $client = new Client();
            $response = $client->post('http://127.0.0.1:8001/api/update-pegawai', ['form_params' =>
            [
                'id' => $request->id,
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'jenis_kelamin' => $request->jenis_kelamin,
                'posisi' => $request->posisi
            ]]);
       
            if ($response) {
                return redirect('/')->with('success', 'Data Berhasil Diedit');
            } else {
                return redirect('/')->with('errors', 'Data Gagal Diedit');
            }
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = new Client();
        $request = $client->delete('127.0.0.1:8001/api/delete-pegawai');
        $response = $request->send();
        if ($delete) {
            return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        } else {
            return redirect()->back()->with('errors', 'Data Gagal Dihapus');
        }
    }

    public function export()
    {
        return Excel::download(new Dataexport, 'data.xlsx');
    }
}
